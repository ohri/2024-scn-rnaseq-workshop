# R Language Introduction

This page contains the content for the Introduction to R class presented by the [Ottawa Bioinformatics Core Facility](https://www.ohri.ca/bioinformatics/) on behalf of the [Stem Cell Network](https://stemcellnetwork.ca/). 

This class is a prerequisite for the [2024 SCN RNA-seq workshop](https://gitlab.com/ohri/2024-scn-rnaseq-workshop) for students who do not have experience with the R programming language. 

## Course content and associated links

The course requires about an hour to complete and consists of a series of videos and associated content, linked below.

It is assumed that you have a computer with R and Rstudio installed. 

## Links: 
* [Download the R software environment here](https://www.r-project.org/)
* [Download Rstudio Desktop here](https://posit.co/downloads/)
* [Download the main R tutorial file: R_walkthrough.Rmd](https://gitlab.com/ohri/2023-scn-rnaseq-workshop/-/raw/main/Files/Rwalkthrough_2023.Rmd?inline=false)

## Video presentations (please view in the order below)

1. [Introduction](http://dropbox.ogic.ca/workshop_2023/R_Introduction_2023.mp4)
2. [R and Rstudio](http://dropbox.ogic.ca/workshop_2023/R_and_Rstudio_2023.mp4)
3. [A walkthrough of basic R capabilities](http://dropbox.ogic.ca/workshop_2023/R_walkthrough_2023.mp4)
4. [Finding and installing R packages](http://dropbox.ogic.ca/workshop_2023/R_packages_2023.mp4)

_Note that this is the same content used in the 2023 workshop so some files reference 2023._