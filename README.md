# 2024 Stem Cell Network RNA-seq Workshop

## Workshop Dates 
* Session 1: May 1,2 Tutorial Day May 3, 11-5PM (EDT)
* Session 2: May 8,9 Tutorial Day May 10, 11-5PM (EDT)

## Workshop Venue
A Zoom meeting link for each session will be sent to participants separately.

## Organizers		
Dr. Bill Stanford (uOttawa/OHRI)  
Dr. Ted Perkins (uOttawa/OHRI)  
Dr. David Cook (uOttawa/OHRI)

## Workshop Files

* [Workshop Agenda](Files/2024_RNA-seq_Analysis_Workshop_Agenda.pdf) 

### R Language Introduction

* Please complete [this one hour](Files/rlangintro.md) online class before your scheduled workshop date if you are not an experienced R programmer


### RNASeq Workshop
_Content will be linked below over the course as it becomes available_

* [Installation instructions for required R packages](Files/R_package_installation.txt)

### Day 1:

Overview and Intro (Dr. Theodore Perkins)[Presentation](Files/2024_Workshop_Lecture_0_PerkinsAM.pdf?inline=FALSE) 
* \[[Session 1 Video](http://dropbox.ogic.ca/workshop_2024/intro.mp4)\] 
* \[[Session 2 Video](http://dropbox.ogic.ca/workshop_2024/session2_perkins_intro.mp4)\] 

RNA-seq Basics (Gareth Palidwor) [Presentation](Files/2024_Workshop_Lecture_1_RNASeq_Basics.pptx?inline=false) 
* \[[Session 1 Video](http://dropbox.ogic.ca/workshop_2024/basics_and_multiqc.mp4)\]
* \[[Session 2 Video](http://dropbox.ogic.ca/workshop_2024/session2_gareth_intro_chris_multiqc.mp4)\]
* Example multiqc file
    * [MultiQC report](http://www.ogic.ca/projects/workshop_2019/multiqc/multiqc_report.html?infile=false) 

Differential Gene Expression with DESeq2 (Christopher Porter) [Presentation](Files/2024_Workshop_Lecture_2_DESeq2.pptx) 
* \[[Session 1 Video](http://dropbox.ogic.ca/workshop_2024/DESeq2.mp4)\]
* \[[Session 2 Video](http://dropbox.ogic.ca/workshop_2024/session2_cporter_deseq2.mp4)\]
* [Analysis walk-through (R Markdown script)](Files/DESeq2_walkthrough.Rmd)
* [GEO page for downloading GSE50499_GEO_Ceman_counts.txt](https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE50499)
* [Analysis script from May 1 session](Files/may_1_analysis.Rmd)

RNA-seq Experimental Design  (Dr. Ted Perkins) [presentation](Files/2024_Workshop_Lecture_PerkinsPM.pdf?inline=false) 
* \[[Session 1 Video](http://dropbox.ogic.ca/workshop_2024/experimentaldesign.mp4)\]
* \[[Session 2 Video](http://dropbox.ogic.ca/workshop_2024/session2_perkins_methods.mp4)\]

### Day 2:
Gene Set Enrichment Analysis [Presentation](Files/2024_Workshop_Lecture_3.pptx?inline=false) 
* \[[Session 1 Video](http://dropbox.ogic.ca/workshop_2024/2024_annotation_enrichment.mp4)\]
* \[[Session 2 Video](http://dropbox.ogic.ca/workshop_2024/session2_gareth_enrichment.mp4)\]
* [Exercise using g:Profiler](Files/Annotation_Enrichment_tutorial_2024.pdf)
* [Files to use for exercise](Files/FilesForEnrichmentExercise.zip)

scRNA-seq (Dr. David Cook) [Presentation](Files/single_cell_workshop_2024.pdf?inline=false) 
* \[[Session 1 Video](http://dropbox.ogic.ca/workshop_2024/singlecell.mp4)\]
* \[[Session 2 Video](http://dropbox.ogic.ca/workshop_2024/session2_david_single_cell.mp4)\]
*  [Notebooks and data](https://github.com/dpcook/scrna_seq_workshop_2023)
